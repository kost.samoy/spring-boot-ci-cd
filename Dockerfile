FROM openjdk:11-jdk

WORKDIR /app

COPY target/test-ci-cd-0.0.1-SNAPSHOT.jar /app/test-ci-cd.jar

EXPOSE 8080

CMD ["java", "-jar", "test-ci-cd.jar"]