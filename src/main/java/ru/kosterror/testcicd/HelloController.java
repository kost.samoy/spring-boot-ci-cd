package ru.kosterror.testcicd;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@RestController
public class HelloController {

    @Value("${app.string}")
    private String str;

    private LocalDateTime localDateTime;

    @PostConstruct
    private void init() {
        localDateTime = LocalDateTime.now();
    }

    @GetMapping("/current-time")
    public String currentTime() {
        return LocalDateTime.now().toString();
    }

    @GetMapping("/start-time")
    public LocalDateTime startTime() {
        return localDateTime;
    }

    @GetMapping("/string")
    public String string() {
        return str;
    }


}
